///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Maxwell Pauly <mgpauly@hawaii.edu>
/// @date    23 Jan 2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

int atoi(const char*);
char *strchr(const char*, int);

int main( int argc, char* argv[] ) {
   int cliNumber; 
   if (argc > 1) {
      //check if number has '.' in it, meaning it is a float
      char* decimalSearchResults;
      decimalSearchResults = strchr(argv[1], '.');
      if (decimalSearchResults) {
         printf("Number Cannot be a float\n");
         return 1;
      }
   
      //check if non number
      cliNumber = atoi(argv[1]);
      if (cliNumber == 0) {
      
         //sets default to 1 if cli input is invalid
         cliNumber = 1;
      }

   }  else {
      cliNumber = 1;
   }
   //guessing game
   srand(time(NULL));
   int number = (rand() % cliNumber) + 1;
   
   printf("OK cat, I'm thinking of a number from 1 to %d. Make a guess: ", cliNumber);
   int guess;
   scanf( "%d", &guess );  

   while(guess != number) {
      if (guess < number) {
         printf("No cat... the number I'm thinking of is larger than %d\n", guess);
   }
      if (guess > number) {
         printf("No cat... the number I'm thinking of is smaller than %d\n", guess);
      }
      printf("OK cat, I'm thinking of a number from 1 to %d. Make a guess: ", cliNumber);
      scanf("%d", &guess);
           
   }
   printf("You got me.\n");
   printf("|\\---/|\n");
   printf("| o_o |\n");
   printf(" \\_^_/\n");
   return 0;
}
